# README #

This is test for Qualium System company. 
Developer: Mark Sokokolovsky

**Installing**

```
git clone project
cd project
npm install
```

**Required**

elasticearch global install
https://www.elastic.co/downloads/elasticsearch

**Starting**

configure host

**Tools**

Backend: elasticsearch 2.3
Frontend: Angular 1.5.8