
function UserDataService(esClient, esResponse, APP_CONFIG){

    return {
        load: load,
        getById: getById,
        create: create,
        update: update,
        remove: remove,
        removeById: removeById,
        login: login,
        getUserByToken: getUserByToken,
    };

    function load(){
        return esClient.search({
            index: APP_CONFIG.index,
            type: APP_CONFIG.type,
        }).then(function(data){
            return Promise.resolve(esResponse.collection(data));
        });
    }

    function getById(id){
        return esClient.search({
            index: APP_CONFIG.index,
            type: APP_CONFIG.type,
            id: id
        }).then(function(data){
            return Promise.resolve(esResponse.first(data));
        });
    }

    function login(params){
        return esClient.search({
            index: APP_CONFIG.index,
            type: APP_CONFIG.type,
            body: {
                "query":{
                    "bool": {
                        "must": [
                            {"match": {"email": params.email}},
                            {"match": {"password":params.password}}
                        ],
                    }
                },
            },
        }).then(function(data){
            return Promise.resolve(esResponse.first(data));
        });
    }

    function getUserByToken(token){
        return esClient.search({
            index: APP_CONFIG.index,
            type: APP_CONFIG.type,
        }).then(function(data){
            for(var i in data.hits.hits){
                if(token != null && token == data.hits.hits[i]._source.token){
                    return Promise.resolve(data.hits.hits[i]);
                }
            }
            throw ("Error token");
        });;
    }

    function create(params){
        return esClient.create({
            index: APP_CONFIG.index,
            type: APP_CONFIG.type,
            method: "POST",
            body: params
        })
    }

    function update(id, params){
        delete params.id;
        return esClient.index({
            index: APP_CONFIG.index,
            type: APP_CONFIG.type,
            id: id,
            body: params
        }).then(function(data){
            return Promise.resolve(data);
        });
    }

    function remove(){
        return esClient.indices.delete({
            index: APP_CONFIG.index,
            type: APP_CONFIG.type,
            method: "DELETE"
        })
    }

    function removeById(id){
        return esClient.delete({
            index: APP_CONFIG.index,
            type: APP_CONFIG.type,
            method: "DELETE",
            id: id
        });
    }

}