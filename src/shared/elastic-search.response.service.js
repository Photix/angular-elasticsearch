function ElasticSearchResponseService(){

    return {
        collection: collection,
        first: first
    };

    function collection(data){
        var data = data.hits.hits;
        var response = [];
        for(var i in data){
            var item = data[i]._source;
            item.id = data[i]._id;
            response.push(item);
        }
        return response;
    }

    function first(data){
        for(var i in data.hits.hits){
            var item = data.hits.hits[i]._source;
            item.id = data.hits.hits[i]._id;
            return item;
        }
        return {};
    }


}