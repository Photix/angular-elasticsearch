
function UserService(localStorageService, $location, $rootScope){

    return {
        setUser: setUser,
        clearUser: clearUser
    };

    function setUser(user){
        $rootScope.auth = true;
        localStorageService.set("user", user);
    }

    function clearUser(user){
        localStorageService.remove("user", user);
        $location.path('/login')
    }

}