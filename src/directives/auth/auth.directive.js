function Auth() {
    return {
        templateUrl: './src/directives/auth/auth.html',
        controller: AuthDirectiveController,
        controllerAs: 'vm'
    }
}


function AuthDirectiveController(localStorageService, $rootScope, $location) {

    var vm = this;

    vm.flags = {};

    vm.handlers = {
        logout: logout
    };

    function logout(){
        localStorageService.remove("user");
        localStorageService.remove("token");
        $rootScope.auth = false;
        $location.path('/login');
    }
}