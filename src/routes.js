angular.module('App').config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "./src/modules/authorization/auth.html",
            controller : "AuthController as vm"
        })
        .when("/login", {
            templateUrl : "./src/modules/authorization/auth.html",
            controller : "AuthController as vm"
        })
        .when("/staff", {
            templateUrl : "./src/modules/user/dashboard.html",
            controller : "UserDashboardController as vm"
        })
        .when("/profile/:id", {
            templateUrl : "./src/modules/user/profile.html",
            controller : "UserProfileController as vm"
        });

    //$locationProvider.html5Mode(true);

});