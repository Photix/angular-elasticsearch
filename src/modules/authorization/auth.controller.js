
function AuthController(
    $scope,
    $location,
    localStorageService,
    userService,
    userDS
){

    var vm = this;

    vm.handlers = {
        registration: registration,
        login: login,
    };
    
    vm.data = {
        login: {},
        registration: {}
    };

    vm.flags = {
        registration: false,
        passwordMatch: true
    };

    function init(){
        console.log('auth init');
    };

    function registration(){
        vm.flags.passwordMatch = true;
        if(vm.data.registration.password != vm.data.registration.passwordConfirm){
            return vm.flags.passwordMatch = false;
        }
        delete vm.data.registration.passwordConfirm;

        vm.data.registration.createDate = "1 january 2000";
        return createUser();
    }

    function createUser(){
        return userDS.create(vm.data.registration).then(enter);
    }

    function enter(){
        return $location.url('/staff');
    }

    function login(){
        userDS.login(vm.data.login)
            .then(setServerUserToken)
            .then(setLocalUserToken)
            .then(goToDashboard)
            .catch(userService.clearUser);
    }

    function setServerUserToken(user){
        if(user && user.email){
            user.token = generateToken();
            vm.data.token = user.token;
            console.log(vm.data.token);
            return userDS.update(user.id, user);
        }
        throw("Authorization error!");
    }

    function setLocalUserToken(){
        return localStorageService.set("token", vm.data.token);
    }

    function goToDashboard(){
        return $location.path('/staff');
    }

    function generateToken(){
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for( var i=0; i < 24; i++ ){
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    init();
}