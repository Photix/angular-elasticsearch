(function () {
    angular.module('app.user', [])

        .controller('UserDashboardController', UserDashboardController)
        .controller('UserProfileController', UserProfileController)

}());