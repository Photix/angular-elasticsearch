
function UserDashboardController(
    $scope,
    $location,
    userDS
){
    var vm = this;

    vm.handlers = {
        deleteUsers: deleteUsers,
        deleteUser: deleteUser,
        goToUserProfile: goToUserProfile
    };
    vm.data = {};

    function init(){
        return loadUsers()
            .then(setUsers);
    }

    function loadUsers(){
        return userDS.load();
    }

    function setUsers(users){
        console.log('i', users);
        vm.data.users = users;
    }

    function deleteUser(id){
        return userDS.removeById(id)
                .then(removeFromTable(id));
    }
    
    function removeFromTable(id){
        var index = _.findIndex(vm.data.users, {id: id});
        vm.data.users[index].hide = true;
    }

    function deleteUsers(){
        return userDS.remove();
    }

    function goToUserProfile(id){
        console.log('id', id);
        $location.path('/profile/'+id);
    }

    function digest(){
        $scope.$digest();
    }

    init();
}