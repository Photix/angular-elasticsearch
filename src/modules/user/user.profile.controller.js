
function UserProfileController(
    $scope,
    $routeParams,
    userDS
){
    var vm = this;

    vm.handlers = {
        updateUser: updateUser,
        addUserNote: addUserNote,
        deleteNote: deleteNote
    };
    vm.data = {};

    function init(){
        return loadUser($routeParams.id)
            .then(setUser);
    }

    function loadUser(id){
        return userDS.getById(id);
    }

    function setUser(user){
        vm.data.user = user;
    }

    function updateUser(){
        userDS.update(vm.data.user.id, vm.data.user).then(updated);
    }

    function addUserNote(){
        if(!vm.data.user.notes) { vm.data.user.notes = [] }
        if(vm.data.note){
            vm.data.user.notes.push(vm.data.note);
            delete vm.data.note;
            userDS.update(vm.data.user.id, vm.data.user);
        }
    }

    function deleteNote(key){
        vm.data.user.notes.splice(key, 1);
        userDS.update(vm.data.user.id, vm.data.user);
    }

    function updated(){
        alert('updated');
    }

    init();
}