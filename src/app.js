angular.module('App', [
    'elasticsearch',
    'ngRoute',
    'LocalStorageModule',

    'app.auth',
    'app.user'
])
    
.run(function(localStorageService, userDS, userService, $rootScope){
    $rootScope.$on('$viewContentLoaded', function(){
        userDS.getUserByToken(localStorageService.get("token"))
            .then(userService.setUser)
            .catch(userService.clearUser);
    });
    console.log("TOKEN", localStorageService.get("token"));
    
})    
    
.factory('esClient', ElasticSearchDataService)
.factory('esResponse', ElasticSearchResponseService)
.factory('userDS', UserDataService)
.factory('userService', UserService)
.factory('userService', UserService)


.directive('auth', Auth)



;